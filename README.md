# Alexa Extension


Ksquare's Alexa Connector 1.0.0 for Mule 4 offers an out-of-the-box solution to integrate Amazon Alexa Skill Management API with other business applications.

Quickly and easily configure modules to access and control account and campaigns data in Mule flows.

In this first release Alexa Connector implements the following features for the MuleSoft-based enterprise solutions:

- Alexa Skill Management API:
    - Update Interaction Model

More and more features will be covered release by release.

Application/Service         |	Version
--                          | --
Mule Runtime	            |  4.X
Alexa SMAPI     |   v. 1.0
Java	                    |  1.8 and later


## Highlights of Alexa Connector Mule 4

It allows users to find, create, modify and control Alexa Skills, and their Interaction Models linked to an authorized Amazon Developer account.

Add this dependency to your application pom.xml


```xml
<groupId>org.mule.modules.alexa</groupId>
<artifactId>mule-alexa-connector</artifactId>
<version>1.0.0</version>
<classifier>mule-plugin</classifier>
```
