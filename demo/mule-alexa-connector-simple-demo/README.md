Amazon Alexa  Connector Demo
====================================
Anypoint Studio demo for Amazon Alexa Connector.


Prerequisites
---------------

* Anypoint Studio 7 with Mule ESB 4.2.1 Runtime.
* Mule Amazon Alexa connector v1.0.0.


How to Run Sample
-----------------

1. Import the project folder demo in Studio.
2. Create an Amazon alexa developer account and an application for Login with Amazon in this url: https://developer.amazon.com/loginwithamazon/console/site/lwa/overview.html
3. Whitelist the hostname for the callbacks in the security profile created in amazon account and obtain your client id and client secret in your Amazon Application. You can do this in this web page:
   https://developer.amazon.com/settings/console/securityprofile/overview.html
4. Create an application.properties file and save it in the src/main/resources/ directory.

Write the following values in the application properties files, with the values obtained in the previous step.

consumer.key= amzn1.application-xx-xxxx-xxxxxxxxxxxxx
consumer.secret= xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
callback.url=http://localhost:8081/callback *(for example)*
alexa.vendorId=XXXXXXXXXXXX

5. Run the application.

6. Navigate to the  http://localhost:8081/authorize endpoint and enter your Amazon account Credentials, check if you see the message token received successfully on browser.
7. Access the endpoints for respective operation like create Skill,Update Skill, Delete Skill etc.


About the Sample
----------------
After successful receive of token in step (4) of above, use the below link to create skill
Using a browser, navigate to:
http://localhost:8081/create for testing the "create Skill" operation

http://localhost:8081/update with the Skill Id as the query parameter "skillId" for testing the "Update the Interaction Model" operation

http://localhost:8081/updateInteraction with the Skill Id as the query parameter "skillId" for testing the "Update the Interaction Model" operation

http://localhost:8081/skill with the Skill Id as the query parameter "skillId" for testing the "Get a Skill Information" operation

http://localhost:8081/delete with the Skill Id as the query parameter "skillId" for testing the "Delete a Skill"

