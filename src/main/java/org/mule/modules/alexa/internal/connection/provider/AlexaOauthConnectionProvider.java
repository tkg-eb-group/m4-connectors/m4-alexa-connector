/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.internal.connection.provider;

import org.mule.modules.alexa.internal.connection.AlexaConnection;
import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.lifecycle.Startable;
import org.mule.runtime.api.lifecycle.Stoppable;
import org.mule.runtime.extension.api.annotation.connectivity.oauth.AuthorizationCode;
import org.mule.runtime.extension.api.connectivity.oauth.AuthorizationCodeState;

@AuthorizationCode(authorizationUrl = "https://www.amazon.com/ap/oa",
		accessTokenUrl = "https://api.amazon.com/auth/o2/token",
defaultScopes = "alexa::ask:skills:readwrite alexa::ask:models:readwrite")
public class AlexaOauthConnectionProvider extends AlexaConnectionProvider implements CachedConnectionProvider<AlexaConnection>, Startable, Stoppable {

	private AuthorizationCodeState state;

	@Override
	public AlexaConnection connect() throws ConnectionException {
		if (state.getAccessToken() == null) {
			throw new ConnectionException("Unable to get aws access token");
		}
		
		return new AlexaConnection(getHttpClient(), getConnectionConfiguration(), state);
	}
    
	@Override
	public void disconnect(AlexaConnection connection) {
		connection.setHttpClient(null);
	}  
   
	@Override
	public ConnectionValidationResult validate(AlexaConnection connection) {
		return ConnectionValidationResult.success();
	}
}


