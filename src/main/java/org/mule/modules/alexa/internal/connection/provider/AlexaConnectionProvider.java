/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.internal.connection.provider;

import org.mule.modules.alexa.internal.config.AlexaConnectionConfiguration;
import org.mule.modules.alexa.internal.connection.AlexaConnection;
import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.lifecycle.Startable;
import org.mule.runtime.api.lifecycle.Stoppable;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.RefName;
import org.mule.runtime.http.api.HttpService;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.client.HttpClientConfiguration;

import javax.inject.Inject;

public abstract class AlexaConnectionProvider implements CachedConnectionProvider<AlexaConnection>, Startable, Stoppable {

    @Inject
    private HttpService httpService;
    private HttpClient httpClient;

    @RefName
    private String configName;

    @ParameterGroup(name = "Connection")
    private AlexaConnectionConfiguration connectionConfiguration;

    @Override
    public void start() {
        HttpClientConfiguration httpClientConfiguration = new HttpClientConfiguration.Builder()
                .setName(configName)
                .setProxyConfig(connectionConfiguration.getProxyConfig())
                .build();
        httpClient = httpService.getClientFactory().create(httpClientConfiguration);
        httpClient.start();
    }

    @Override
    public void stop() {
        if (httpClient != null) {
            httpClient.stop();
        }
    }

    @Override
    public void disconnect(AlexaConnection connection) {
        connection.setHttpClient(null);
    }

    public HttpService getHttpService() {
        return httpService;
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public String getConfigName() {
        return configName;
    }

    public AlexaConnectionConfiguration getConnectionConfiguration() {
        return connectionConfiguration;
    }
}
