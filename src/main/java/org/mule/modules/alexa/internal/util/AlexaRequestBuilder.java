/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.internal.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.mule.runtime.http.api.domain.entity.ByteArrayHttpEntity;
import org.mule.runtime.http.api.domain.entity.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AlexaRequestBuilder {

	private static final Logger logger = LoggerFactory.getLogger(AlexaRequestBuilder.class);

	private static final ObjectMapper objectMapper = new ObjectMapper();

	private AlexaRequestBuilder() {
		objectMapper.setSerializationInclusion(Include.NON_NULL);
	}

	public static HttpEntity objectToEntity(Object object) throws JsonProcessingException {

		String jsonAsString = objectMapper
				.setSerializationInclusion(Include.NON_NULL)
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(object);
		logger.info(jsonAsString);

		return  new ByteArrayHttpEntity(jsonAsString.getBytes(StandardCharsets.UTF_8));
	}

	public static String inputStreamToString(InputStream inputStream) throws IOException {

		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

			byte[] buffer = new byte[1024];
			int length;

			while ((length = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, length);
			}

			return outputStream.toString("UTF-8");
		}
	}
	public static String cleanMuleFormat(String jsonString) {
		jsonString = jsonString.replace("\n", "").replace("\r", "");
		Pattern pattern = Pattern.compile("\\#\\[(.*?)\\]$");
		Matcher matcher = pattern.matcher(jsonString);
		matcher.find();
		String cleaned = matcher.group(1);
		logger.debug(cleaned);
		return cleaned;
	}

}
