/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.internal.error;

import static java.util.Optional.ofNullable;
import static org.mule.runtime.extension.api.error.MuleErrors.*;

import java.util.Optional;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;


public enum AlexaApiErrorType implements ErrorTypeDefinition<AlexaApiErrorType> {

	
	EXECUTION,
	JSON_PARSER_EXCEPTION(EXECUTION);

	AlexaApiErrorType(){
		
	}
	 
	
    private ErrorTypeDefinition<?> parent;

    AlexaApiErrorType(final ErrorTypeDefinition<?> parent) {
        this.parent = parent;
    }

   
    @Override
    public Optional<ErrorTypeDefinition<? extends Enum<?>>> getParent() {
        return ofNullable(parent);
    }
	
	
	 
	
}
