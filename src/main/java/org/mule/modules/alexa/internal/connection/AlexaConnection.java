/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.internal.connection;

import org.mule.modules.alexa.internal.util.AlexaConstants;
import org.mule.modules.alexa.internal.config.AlexaConnectionConfiguration;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.extension.api.connectivity.oauth.AuthorizationCodeState;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * This class represents an extension connection just as example (there is no
 * real connection with anything here c:).
 */
public class AlexaConnection {
	private static final Logger logger = LoggerFactory.getLogger(AlexaConnection.class);

	private HttpClient httpClient;
	private String accessToken;
	private int timeout;
	private TimeUnit timeoutUnit;
	private String vendorId;
	private AuthorizationCodeState state;

	public AlexaConnection(HttpClient httpClient, AlexaConnectionConfiguration connectionConfiguration,
						   AuthorizationCodeState state){
		this.accessToken = state.getAccessToken();
		this.httpClient = httpClient;
		this.timeout = connectionConfiguration.getTimeout();
		this.timeoutUnit = connectionConfiguration.getTimeoutUnit();
		this.state = state;
	}

	public AlexaConnection(HttpClient httpClient, AlexaConnectionConfiguration connectionConfiguration,
						   String accessToken, String vendorId){
		this.vendorId = vendorId;
		this.accessToken = accessToken;
		this.httpClient = httpClient;
		this.timeout = connectionConfiguration.getTimeout();
		this.timeoutUnit = connectionConfiguration.getTimeoutUnit();
	}

	public boolean isConnected() throws ConnectionException, IOException, TimeoutException {
		HttpRequest request = HttpRequest.builder()
				.method(HttpConstants.Method.GET)
				.uri(String.format(AlexaConstants.LIST_ALEXA_SKILLS, vendorId.trim()))
				.addHeader("Authorization", "Bearer " + getAccessToken())
				.build();

		HttpResponse httpResponse = httpClient.send(request, getTimeoutAsMilliseconds(), false, null);

		if (httpResponse.getStatusCode() >= 200 && httpResponse.getStatusCode() < 300) {
			return true;
		}else {
			ConnectionException e =  new ConnectionException(
					"Error connecting to the server with test vendor id: " + vendorId + " and Error Code: " + httpResponse.getStatusCode() + "~" + httpResponse

			);
			logger.error("Connection error occurred in AlexaConnection::isConnected", e);
			throw e;
		}
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}


	public int getTimeout() {
		return timeout;
	}

	public TimeUnit getTimeoutUnit() {
		return timeoutUnit;
	}

	public int getTimeoutAsMilliseconds(){
		return (int)TimeUnit.MILLISECONDS.convert(getTimeout(), getTimeoutUnit());
	}

	public String getAccessToken() {
		if (state != null){
			return state.getAccessToken();
		}
		return accessToken;
	}

	public String getVendorId() {
		return vendorId;
	}

	public String getResourceOwnerId() {
		if (state != null && state.getResourceOwnerId() != null){
			return state.getResourceOwnerId();
		}
		return "Not Provided.";
	}

}
