/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.internal.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.IOUtils;
import org.mule.modules.alexa.api.domain.data.Attributes;
import org.mule.modules.alexa.internal.connection.AlexaConnection;
import org.mule.modules.alexa.internal.error.AlexaApiErrorType;
import org.mule.modules.alexa.internal.error.exceptions.AlexaApiException;
import org.mule.modules.alexa.internal.util.AlexaRequestBuilder;
import org.mule.runtime.extension.api.connectivity.oauth.AccessTokenExpiredException;
import org.mule.runtime.extension.api.error.MuleErrors;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.request.HttpRequestBuilder;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeoutException;

public class HttpCallService {

    private static final Logger logger = LoggerFactory.getLogger(HttpCallService.class);

    private HttpCallService() {
        // Public constructor
    }

    public static Result<InputStream, Attributes> sendRequest(AlexaConnection connection, HttpConstants.Method method,
                                                              String uri, Object content) {
        logger.info("Sending request to Alexa with uri {} , method {}, content {}", uri, method, content);
        int timeout = connection.getTimeoutAsMilliseconds();
        HttpClient client = connection.getHttpClient();

        HttpRequestBuilder builder = HttpRequest.builder()
                .method(method)
                .uri(uri)
                .addHeader("Authorization", "Bearer " + connection.getAccessToken());
            if (content != null) {
                try {
                    builder.entity(AlexaRequestBuilder.objectToEntity(content));
                } catch (JsonProcessingException e) {
                    logger.error("JSON Parsing error occurred in HttpCallService::call", e);
                    throw new AlexaApiException("Error parsing body to JSON", AlexaApiErrorType.JSON_PARSER_EXCEPTION);
                }
            }

            HttpResponse httpResponse;
            Attributes attributes = new Attributes();
            byte[] response;

            HttpRequest request = builder.build();
            if (logger.isDebugEnabled()){
                logger.debug("Http Request Object: \n");
                logger.debug(request.toString());
            }
            try {
                httpResponse = client.send(request, timeout, true, null);
                attributes.setHeaders(httpResponse.getHeaders());
                attributes.setStatusCode(httpResponse.getStatusCode());
                response = IOUtils.toByteArray(httpResponse.getEntity().getContent());
                if (logger.isDebugEnabled()){
                    logger.debug(String.format("HttpCallService::sendRequest: response String: %n %s", getHttpResponse(new ByteArrayInputStream(response))));
                }

                int statusCode = httpResponse.getStatusCode();
                String reasonPhrase = httpResponse.getReasonPhrase();
                String httpResponseStr;
                httpResponseStr = getHttpResponse(new ByteArrayInputStream(response));
                if (statusCode == 401 && httpResponseStr.contains("Token is invalid/expired")){
                    throw new AccessTokenExpiredException(connection.getResourceOwnerId());
                } else if (statusCode >= 300) {
                    throw new AlexaApiException("Http response status code: " + statusCode + " - " + reasonPhrase
                            + "\n" + httpResponseStr, AlexaApiErrorType.EXECUTION);
                }

            } catch (IOException e) {
                logger.error("Connection error occurred in HttpCallService::call", e);
                throw new AlexaApiException("Error calling HTTP client", MuleErrors.CONNECTIVITY);
            } catch (TimeoutException e) {
                logger.error("Timeout error occurred in HttpCallService::call", e);
                throw new AlexaApiException("Timeout calling HTTP client", MuleErrors.CONNECTIVITY);
            }

        return Result.<InputStream, Attributes>builder().attributes(attributes).output(new ByteArrayInputStream(response)).build();

    }


    private static String getHttpResponse(InputStream httpResponseStream) {
        String httpResponseStr;
        try {
            httpResponseStr = AlexaRequestBuilder.inputStreamToString(httpResponseStream);
        } catch (IOException e) {
            logger.error("Error occurred in HttpCallService::getHttpResponse", e);
            httpResponseStr = "There was an error parsing the reason phrase or it was empty.";
        }
        return httpResponseStr;
    }

}
