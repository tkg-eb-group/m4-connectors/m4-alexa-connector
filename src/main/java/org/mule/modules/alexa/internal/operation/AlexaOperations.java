/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.internal.operation;

import org.mule.modules.alexa.api.domain.data.Attributes;
import org.mule.modules.alexa.api.domain.data.LocaleEnum;
import org.mule.modules.alexa.api.domain.data.StagesEnum;
import org.mule.modules.alexa.api.parameter.request.UpdateInteractionRequest;
import org.mule.modules.alexa.api.parameter.request.UpdateSkillRequest;
import org.mule.modules.alexa.internal.error.provider.BodyErrorTypeProvider;
import org.mule.modules.alexa.internal.error.provider.ExecutionErrorTypeProvider;
import org.mule.modules.alexa.internal.util.AlexaConstants;
import org.mule.modules.alexa.api.parameter.request.CreateSkillRequest;
import org.mule.modules.alexa.internal.connection.AlexaConnection;
import org.mule.modules.alexa.internal.service.HttpCallService;
import org.mule.modules.alexa.internal.util.ExpressionResolver;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.http.api.HttpConstants;

import java.io.InputStream;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;
import static org.mule.runtime.api.meta.ExpressionSupport.SUPPORTED;

/**
 * This class is a container for operations, every public method in this class
 * will be taken as an extension operation.
 */

public class AlexaOperations {

	/**
	 * This method used for creating skill with basic information later we can
	 * update with full information using updateSkillManifest,updateSkillManifest
	 * and updateSkill operations, return error received from Alexa Server if some
	 * thing wrong in create request or missed some fields which are required
	 * for skill creations.
	 *  
	 * @param alexaConnection
	 * 		  connection to the alexa Skills API
	 * @param createSkillBody
	 * 			The body includes the vendorId, and a manifest object as described in Skill Manifest
	 * @return InputStream
	 * 			Json response from Alexa sever
	 */
	@OutputJsonType(schema = "metadata/create-skill-schema.json")
	@MediaType(value = "application/json")
	@Throws(BodyErrorTypeProvider.class)
	@Summary("Operation that submits a skill creation request for a specified vendorId")
	@DisplayName("Create a skill")
	public Result<InputStream, Attributes>  createSkill(@Connection AlexaConnection alexaConnection,
								   @Expression(SUPPORTED) @ParameterDsl(allowReferences = false)
										   ParameterResolver<CreateSkillRequest> createSkillBody) {
		ExpressionResolver<CreateSkillRequest> resolver = new ExpressionResolver<>();
		CreateSkillRequest createSkillRequest = resolver.resolve(createSkillBody, CreateSkillRequest.class);
		return HttpCallService.sendRequest(
				alexaConnection,
				HttpConstants.Method.POST,
				AlexaConstants.CREATE_ALEXA_SKILL,
				createSkillRequest);
	}

	/**
	 * This method return skill information for the given skillId.
	 *
	 * @param alexaConnection
	 * 		  connection to the alexa Skills API
	 * @param skillId
	 * 		ID of the created alexa skill to modify
	 * @param stage
	 * 		Stage of the skill
	 * @return InputStream
	 *       skill information in json format
	 */
	@OutputJsonType(schema = "metadata/skill-manifest-schema.json")
	@MediaType(value = "application/json")
	@Throws(ExecutionErrorTypeProvider.class)
	@Summary("Operation that gets the manifest for a specified skill.")
	@DisplayName("Get skill information")
	public Result<InputStream, Attributes>  getSkillInformation(@Connection AlexaConnection alexaConnection, @Expression(SUPPORTED) String skillId,
																@Expression(NOT_SUPPORTED) StagesEnum stage) {
		String url = String.format(AlexaConstants.GET_ALEXA_INFO, skillId, stage.getValue());
		return HttpCallService.sendRequest(alexaConnection, HttpConstants.Method.GET, url, null);
	}

	/**
	 * This method deletes the skill for the given skillId
	 * @param alexaConnection
	 * 		  connection to the alexa Skills API
	 * @param skillId
	 * 		ID of the created alexa skill to modify
	 * @return empty {@code : {}}
	 */
	@MediaType(value = "application/json")
	@Summary("Operation that deletes a skill.")
	@Throws(ExecutionErrorTypeProvider.class)
	@DisplayName("Delete a skill")
	public Result<InputStream, Attributes> deleteSkill(@Connection AlexaConnection alexaConnection, @Expression(SUPPORTED) String skillId) {
		String deleteUrl = String.format(AlexaConstants.DELETE_SKILL, skillId);
		return HttpCallService.sendRequest(alexaConnection, HttpConstants.Method.DELETE, deleteUrl, null);
	}

	/**
	 * This method updates the Manifest schema of skill, refer the manifest schema
	 * of Alexa skill for more information
	 * @see <a href="https://developer.amazon.com/docs/smapi/skill-manifest.html">Skill Manifest</a>
	 *
	 * @param alexaConnection
	 * 		  connection to the alexa Skills API
	 * @param skillId
	 *            SkillId of existing skill
	 * @param updateSkillBody
	 *            Manifest model
	 * @return empty json object
	 */
	@MediaType(value = "application/json")
	@Throws(BodyErrorTypeProvider.class)
	@Summary("Operation that updates the skill Manifest associated with the specified skill ID.")
	@DisplayName("Update an existing skill")
	@OutputJsonType(schema = "metadata/empty-response-schema.json")
	public Result<InputStream, Attributes>  updateSkill(@Connection AlexaConnection alexaConnection,
								   @Expression(SUPPORTED) String skillId,
								   @Expression(SUPPORTED) @ParameterDsl(allowReferences = false)
											   ParameterResolver<UpdateSkillRequest> updateSkillBody) {
		String url = String.format(AlexaConstants.UPDATE_SKILL_MANIFEST, skillId);
		ExpressionResolver<UpdateSkillRequest> resolver = new ExpressionResolver<>();
		UpdateSkillRequest updateSkillRequest = resolver.resolve(updateSkillBody, UpdateSkillRequest.class);
		return HttpCallService.sendRequest(
				alexaConnection,
				HttpConstants.Method.PUT,
				url,
				updateSkillRequest);
	}

	/**
	 * This is used to update interaction model schema of the Alexa skill.
	 * @param alexaConnection
	 * 		  connection to the alexa Skills API
	 * @param skillId
	 * 		ID of the created alexa skill to modify
	 * @param stage
	 * 	    Stage of the skill
	 * @param locale
	 * 	    Locale of interaction model being updated
	 * @param updateInteractionRequestBody
	 * 	    The interaction model
	 * @return InputStream
	 */
	@MediaType(value = "application/json")
	@Throws(BodyErrorTypeProvider.class)
	@Summary("Operation used to update the interaction model for a specified skill.")
	@DisplayName("Update interaction model")
	@OutputJsonType(schema = "metadata/empty-response-schema.json")
	public Result<InputStream, Attributes>  updateInteractionModel(@Connection AlexaConnection alexaConnection,
																		   @Expression(NOT_SUPPORTED) StagesEnum stage,
																		   @Expression(SUPPORTED) String skillId,
																		   @Expression(NOT_SUPPORTED) LocaleEnum locale,
																		   @Expression(SUPPORTED) @ParameterDsl(allowReferences = false)
											  ParameterResolver<UpdateInteractionRequest> updateInteractionRequestBody) {
		ExpressionResolver<UpdateInteractionRequest> resolver = new ExpressionResolver<>();
		UpdateInteractionRequest updateInteractionRequest = resolver.resolve(updateInteractionRequestBody, UpdateInteractionRequest.class);
		String url = String.format(AlexaConstants.UPDATE_SKILL_INTERACTION_SCHEMA, skillId, stage.getValue(), locale.getValue());
		return HttpCallService.sendRequest(alexaConnection, HttpConstants.Method.PUT, url,
				updateInteractionRequest);
	}
}
