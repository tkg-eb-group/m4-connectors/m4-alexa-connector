/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.internal.connection.provider;

import org.mule.modules.alexa.internal.connection.AlexaConnection;
import org.mule.modules.alexa.internal.error.exceptions.AlexaApiException;
import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.lifecycle.Startable;
import org.mule.runtime.api.lifecycle.Stoppable;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.error.MuleErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Alias("token")
public class TokenConnectionProvider extends AlexaConnectionProvider implements CachedConnectionProvider<AlexaConnection>, Startable, Stoppable {

	private static final Logger logger = LoggerFactory.getLogger(TokenConnectionProvider.class);

	@Parameter
	private String accessToken;

	@Parameter
	@Optional
	@Placement(order = 1)
	@Expression(ExpressionSupport.SUPPORTED)
	private String testVendorId;

	@Override
	public AlexaConnection connect() throws ConnectionException {
		return new AlexaConnection(getHttpClient(), getConnectionConfiguration(), accessToken, testVendorId);
	}

	@Override
	public ConnectionValidationResult validate(AlexaConnection connection) {
		ConnectionValidationResult result;
		try {
			connection.isConnected();
			result = ConnectionValidationResult.success();
		} catch (Exception e) {
			logger.error("Connection error occurred in TokenConnectionProvider::validate", e);
			result = ConnectionValidationResult.failure("Connection Failed: " + e.getMessage(),
					new AlexaApiException("Error calling HTTP client", MuleErrors.CONNECTIVITY));
		}
		return result;
	}
}
