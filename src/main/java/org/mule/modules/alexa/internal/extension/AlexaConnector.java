/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.internal.extension;

import org.mule.modules.alexa.api.connection.DefaultProxyConfig;
import org.mule.modules.alexa.internal.error.AlexaApiErrorType;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.modules.alexa.internal.config.AlexaConfiguration;
import org.mule.runtime.api.meta.Category;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.SubTypeMapping;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;
import org.mule.runtime.extension.api.annotation.error.ErrorTypes;
import org.mule.runtime.extension.api.annotation.license.RequiresEnterpriseLicense;
import org.mule.runtime.http.api.client.proxy.ProxyConfig;

/**
 * This is the main class of an extension, is the entry point from which
 * configurations, connection providers, operations and sources are going to be
 * declared.<<<<<<<<<<<<<<<<<<<<<
 */

@Xml(prefix = "alexa")
@Extension(name = "Alexa", vendor = "com.ksquaregroup", category=Category.CERTIFIED)
@RequiresEnterpriseLicense(allowEvaluationLicense = true)
@Configurations(AlexaConfiguration.class)
@ErrorTypes(AlexaApiErrorType.class)
@SubTypeMapping(baseType = ProxyConfig.class, subTypes = {DefaultProxyConfig.class})
public class AlexaConnector {

}
