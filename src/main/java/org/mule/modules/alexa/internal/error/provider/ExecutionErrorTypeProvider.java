/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.internal.error.provider;

import org.mule.modules.alexa.internal.error.AlexaApiErrorType;
import org.mule.runtime.extension.api.annotation.error.ErrorTypeProvider;
import org.mule.runtime.extension.api.error.ErrorTypeDefinition;


import java.util.HashSet;
import java.util.Set;

public class ExecutionErrorTypeProvider implements ErrorTypeProvider {

	@Override
	public Set<ErrorTypeDefinition> getErrorTypes() {
		return new HashSet<>(getAlexaErrorTypes());
	}

	public Set<ErrorTypeDefinition<AlexaApiErrorType>> getAlexaErrorTypes(){
		HashSet<ErrorTypeDefinition<AlexaApiErrorType>> errors = new HashSet<>();
		errors.add(AlexaApiErrorType.EXECUTION);
		return errors;
	}
}
