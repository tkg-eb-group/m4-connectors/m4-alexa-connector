/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.internal.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mule.modules.alexa.internal.error.AlexaApiErrorType;
import org.mule.modules.alexa.internal.error.exceptions.AlexaApiException;

import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpressionResolver<T> {

    private static final Logger logger = LoggerFactory.getLogger(ExpressionResolver.class);

    public T resolve(ParameterResolver<T> input, Class<T> type){
        T output;
        java.util.Optional<String> optionalExpression = input.getExpression();
        if (optionalExpression.isPresent()){
            try {
                String jsonString = optionalExpression.get();
                jsonString = AlexaRequestBuilder.cleanMuleFormat(jsonString);
                ObjectMapper objectMapper = new ObjectMapper();
                output = objectMapper.readValue(jsonString, type);
            } catch (JsonProcessingException | IllegalStateException | NullPointerException e) {
                logger.error("Connection error occurred in ExpressionResolver::resolve", e);
                throw new AlexaApiException("Json parsing to object failure.", AlexaApiErrorType.JSON_PARSER_EXCEPTION);
            }
        } else {
            output = input.resolve();
        }
        return output;
    }
}
