/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.internal.config;

import org.mule.modules.alexa.internal.connection.provider.AlexaOauthConnectionProvider;
import org.mule.modules.alexa.internal.connection.provider.TokenConnectionProvider;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Configuration;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.http.api.client.proxy.ProxyConfig;

import java.util.concurrent.TimeUnit;

@Configuration(name = "connectionConfig")
@ConnectionProviders({TokenConnectionProvider.class, AlexaOauthConnectionProvider.class})
public class AlexaConnectionConfiguration {


    @Parameter
    @Optional(defaultValue = "5")
    @DisplayName("Timeout")
    @Placement(tab = Placement.ADVANCED_TAB, order = 1)
    @Expression(ExpressionSupport.SUPPORTED)
    private int timeout;

    @Parameter
    @Optional(defaultValue = "SECONDS")
    @DisplayName("Timeout unit")
    @Placement(tab = Placement.ADVANCED_TAB, order = 2)
    @Expression(ExpressionSupport.SUPPORTED)
    private TimeUnit timeoutUnit;

    @Parameter
    @Optional
    @DisplayName("Proxy configuration")
    @Placement(tab = "Proxy", order = 3)
    private ProxyConfig proxyConfig;


    public int getTimeout() {
        return timeout;
    }

    public TimeUnit getTimeoutUnit() {
        return timeoutUnit;
    }

    public ProxyConfig getProxyConfig() {
        return proxyConfig;
    }
}
