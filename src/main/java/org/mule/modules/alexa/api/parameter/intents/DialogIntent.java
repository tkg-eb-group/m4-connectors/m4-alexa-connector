/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class DialogIntent implements Serializable {

	@Parameter
	@JsonProperty("name")
	private String intentName;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonProperty("delegationStrategy")
	@Optional
	private DelegationStrategy intentDelegationStrategy;

	@Parameter
	@Optional
	@Expression(ExpressionSupport.SUPPORTED)
	private boolean confirmationRequired;

	@Parameter
	@Optional
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonProperty("slots")
	private List<DialogSlot> dialogSlots;

	@Parameter
	@Optional
	@NullSafe
	@Expression(ExpressionSupport.REQUIRED)
	@DisplayName("Prompts")
	@JsonProperty("prompts")
	private Map<String, String> dialogPrompts;

	public DialogIntent() {
		// Default constructor
	}

	public String getIntentName() {
		return intentName;
	}

	public List<DialogSlot> getDialogSlots() {
		return dialogSlots;
	}

	public Map<String, String> getDialogPrompts() {
		return dialogPrompts;
	}

	public DelegationStrategy getIntentDelegationStrategy() {
		return intentDelegationStrategy;
	}

	public boolean isConfirmationRequired() {
		return confirmationRequired;
	}
}
