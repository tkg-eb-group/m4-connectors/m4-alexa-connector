/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class Dialog implements Serializable {

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@DisplayName("Intents")
	@JsonProperty("intents")
	private List<DialogIntent> dialogIntents;

	@Parameter
	@Expression(ExpressionSupport.SUPPORTED)
	private DelegationStrategy delegationStrategy = DelegationStrategy.ALWAYS;

	public Dialog() {
		//Default constructor
	}

	public List<DialogIntent> getDialogIntents() {
		return dialogIntents;
	}

	public DelegationStrategy getDelegationStrategy() {
		return delegationStrategy;
	}
}
