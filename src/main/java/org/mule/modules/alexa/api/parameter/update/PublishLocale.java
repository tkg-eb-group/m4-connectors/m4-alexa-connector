/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.parameter.update;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class PublishLocale implements Serializable {

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonProperty("name")
	private String name;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	private String summary;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	@JsonProperty("description")
	private String description;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	private List<String> keywords;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	private List<String> examplePhrases;



	public PublishLocale() {
		// Default constructor
	}

	public String getName() {
		return name;
	}

	public String getSummary() {
		return summary;
	}

	public String getDescription() {
		return description;
	}
	public List<String> getKeywords() {
		return keywords;
	}

	public List<String> getExamplePhrases() {
		return examplePhrases;
	}
}
