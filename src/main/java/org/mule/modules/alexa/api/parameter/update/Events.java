/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.update;

import java.io.Serializable;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class Events implements Serializable {


	public Events() {
		// Public constructor
	}

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private Endpoint endpoint;

	@DisplayName("Subscriptions")
	@Parameter
	@Expression(ExpressionSupport.REQUIRED)
	@Optional
	private List<Map<String, String>> subscriptions;

	@DisplayName("Regions")
	@Parameter
	@Expression(ExpressionSupport.REQUIRED)
	@Optional
	private Map<String, RegionEndpoint> regions;

	@DisplayName("Publications")
	@Parameter
	@Expression(ExpressionSupport.REQUIRED)
	@Optional
	private List<Map<String, String>> publications;

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public List<Map<String, String>> getSubscriptions() {
		return subscriptions;
	}

	public Map<String, RegionEndpoint> getRegions() {
		return regions;
	}

	public List<Map<String, String>> getPublications() {
		return publications;
	}
}
