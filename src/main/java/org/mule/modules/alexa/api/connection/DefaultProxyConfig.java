/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.api.connection;

import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.dsl.xml.TypeDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Password;
import org.mule.runtime.http.api.client.proxy.ProxyConfig;

@Alias("proxy")
@TypeDsl(allowTopLevelDefinition = true)
public class DefaultProxyConfig implements ProxyConfig {
    
    @Parameter
    private String host;
    
    @Parameter
    private int port = Integer.MAX_VALUE;
    
    @Parameter
    @Optional
    private String username;
    
    @Parameter
    @Optional
    @Password
    private String password;
    
    @Parameter
    @Optional
    private String nonProxyHosts;

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getNonProxyHosts() {
        return nonProxyHosts;
    }
    
}
