/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.update;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier",
		"exportCompliant",
		"childDirected"
})
public class PrivacyAndCompliance implements Serializable {

	@Parameter
	@Expression
	private boolean allowsPurchases;

	@Parameter
	@Expression
	private boolean usesPersonalInfo;

	@Parameter
	@Expression
	@JsonProperty(value = "isChildDirected")
	private Boolean isChildDirected;

	@Parameter
	@Expression
	@JsonProperty(value = "isExportCompliant")
	private Boolean isExportCompliant;

	@Parameter
	@Expression
	private boolean containsAds;

	@Parameter
	@Expression(ExpressionSupport.REQUIRED)
	@DisplayName("Locales")
	@Optional
	@JsonProperty("locales")
	private Map<String, PrivacyLocaleContent> privacyLocales = new HashMap<>();
	
	public PrivacyAndCompliance() {
		// Default constructor
	}

	public boolean isAllowsPurchases() {
		return allowsPurchases;
	}

	public boolean isUsesPersonalInfo() {
		return usesPersonalInfo;
	}

	public Boolean isIsChildDirected() {
		return isChildDirected;
	}

	public Boolean isIsExportCompliant() {
		return isExportCompliant;
	}

	public boolean isContainsAds() {
		return containsAds;
	}

	public Map<String, PrivacyLocaleContent> getPrivacyLocales() {
		return privacyLocales;
	}
}
