/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class InteractionModel implements Serializable {

	@Parameter
	@Optional
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private Dialog dialog;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private LanguageModel languageModel;

	@Parameter
	@Optional
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<Prompt> prompts = new ArrayList<>();

	public InteractionModel() {
		// Default constructor
	}

	public Dialog getDialog() {
		return dialog;
	}

	public LanguageModel getLanguageModel() {
		return languageModel;
	}

	public List<Prompt> getPrompts() {
		return prompts;
	}
}
