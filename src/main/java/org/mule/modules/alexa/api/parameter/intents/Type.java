/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.api.parameter.intents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

import java.io.Serializable;
import java.util.List;
@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class Type implements Serializable {

    @Parameter
    @Expression(ExpressionSupport.SUPPORTED)
    @DisplayName("Name")
    @JsonProperty("name")
    private String typeName;

    @Parameter
    @Optional
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private List<Value> values;

    public Type() {
        // Default constructor
    }

    public String getTypeName() {
        return typeName;
    }

    public List<Value> getValues() {
        return values;
    }
}
