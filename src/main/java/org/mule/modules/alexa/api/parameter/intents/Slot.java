/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class Slot implements Serializable {

	@Parameter
	@JsonProperty("name")
	@Expression(ExpressionSupport.SUPPORTED)
	private String slotName;

	@Parameter
	@Optional
	@NullSafe
	@Expression(ExpressionSupport.SUPPORTED)
	private String type;

	@Parameter
	@Optional
	@NullSafe
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonProperty("samples")
	@DisplayName("Samples")
	private List<String> slotSamples;

	public Slot() {
		// Default constructor
	}

	public String getSlotName() {
		return slotName;
	}

	public String getType() {
		return type;
	}

	public List<String> getSlotSamples() {
		return slotSamples;
	}
}
