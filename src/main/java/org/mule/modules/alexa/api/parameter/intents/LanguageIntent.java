/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class LanguageIntent implements Serializable {

	@Parameter
	@JsonProperty("name")
	private String intentName;

	@Parameter
	@Optional
	@NullSafe
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private List<Slot> slots = new ArrayList<>();

	@Parameter
	@Optional
	@NullSafe
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonProperty("samples")
	@DisplayName("Samples")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<String> intentSamples = new ArrayList<>();

	public LanguageIntent() {
		// Default constructor
	}

	public String getIntentName() {
		return intentName;
	}

	public List<Slot> getSlots() {
		return slots;
	}

	public List<String> getIntentSamples() {
		return intentSamples;
	}
}
