/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class Prompt implements Serializable {

	@Parameter
	private String id;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private List<Variation> variations;

	public Prompt() {
		// Default constructor
	}

	public String getId() {
		return id;
	}

	public List<Variation> getVariations() {
		return variations;
	}
}
