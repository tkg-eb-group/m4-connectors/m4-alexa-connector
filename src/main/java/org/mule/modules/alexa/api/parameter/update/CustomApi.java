/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.parameter.update;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class CustomApi implements Serializable {

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private Endpoint endpoint;

	@Parameter
	@Optional
	@Expression(ExpressionSupport.REQUIRED)
	private List<Map<String, String>> interfaces;

	public CustomApi() {
		// Public constructor
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public List<Map<String, String>> getInterfaces() {
		return interfaces;
	}
}
