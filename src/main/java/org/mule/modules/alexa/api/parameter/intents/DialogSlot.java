/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.parameter.intents;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class DialogSlot implements Serializable {

	@Parameter
	@Expression(ExpressionSupport.SUPPORTED)
	@JsonProperty("name")
	private String slotName;

	@Optional
	@Parameter
	@Expression(ExpressionSupport.SUPPORTED)
	private String type;

	@Optional
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@DisplayName("Prompts")
	@JsonProperty("prompts")
	private SlotPrompt slotPrompts;

	@Optional
	@Parameter
	@Expression(ExpressionSupport.SUPPORTED)
	private boolean elicitationRequired;

	@Optional
	@Parameter
	@Expression(ExpressionSupport.SUPPORTED)
	private boolean confirmationRequired;

	@Optional
	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<Validation> validations;

	public DialogSlot() {
		// Default constructor
	}

	public boolean isElicitationRequired() {
		return elicitationRequired;
	}

	public boolean isConfirmationRequired() {
		return confirmationRequired;
	}

	public List<Validation> getValidations() {
		return validations;
	}

	public String getSlotName() {
		return slotName;
	}

	public String getType() {
		return type;
	}

	public SlotPrompt getSlotPrompts() {
		return slotPrompts;
	}
}
