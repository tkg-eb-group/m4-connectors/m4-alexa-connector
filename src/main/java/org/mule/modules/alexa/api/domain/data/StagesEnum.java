/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.domain.data;

public enum StagesEnum {

	DEVELOPMENT("development"), LIVE("live");

	private final String value;

	StagesEnum(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
