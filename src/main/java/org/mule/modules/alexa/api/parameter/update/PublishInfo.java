/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.update;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.modules.alexa.api.domain.data.CategoryEnum;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class PublishInfo implements Serializable {

	@Parameter
	@Expression
	@JsonProperty("isAvailableWorldwide")
	private boolean isAvailableWorldwide = true;

	@Parameter
	@Expression
	private String testingInstructions;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	private CategoryEnum category;

	@Parameter
	@Expression(ExpressionSupport.NOT_SUPPORTED)
	@Optional
	private List<String> distributionCountries;

	@Parameter
	@Expression(ExpressionSupport.REQUIRED)
	@DisplayName("Locales")
	@Optional
	@JsonProperty("locales")
	private Map<String, PublishLocale> publishLocales;


	public PublishInfo() {
		// Default constructor
	}

	@JsonProperty("isAvailableWorldwide")
	public boolean isAvailableWorldwide() {
		return isAvailableWorldwide;
	}

	public String getTestingInstructions() {
		return testingInstructions;
	}

	public CategoryEnum getCategory() {
		return category;
	}

	public List<String> getDistributionCountries() {
		return distributionCountries;
	}

	@JsonProperty("locales")
	public Map<String, PublishLocale> getPublishLocales() {
		return publishLocales;
	}
}
