/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.api.parameter.intents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import java.io.Serializable;
import java.util.List;
@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class Validation implements Serializable {

    @Parameter
    @Expression(ExpressionSupport.SUPPORTED)
    private String type;

    @Parameter
    @Expression(ExpressionSupport.SUPPORTED)
    private String prompt;

    @Parameter
    @Optional
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @JsonProperty("values")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> validationValues;

    public String getType() {
        return type;
    }

    public String getPrompt() {
        return prompt;
    }

    public List<String> getValidationValues() {
        return validationValues;
    }

    public Validation() {
        // Default constructor
    }
}
