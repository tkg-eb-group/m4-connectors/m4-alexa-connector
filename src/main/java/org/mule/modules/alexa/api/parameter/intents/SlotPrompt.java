/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.api.parameter.intents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import java.io.Serializable;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({
        "representation",
        "annotations",
        "rootContainerLocation",
        "dslSource",
        "location",
        "identifier"
})
public class SlotPrompt implements Serializable {

    @Parameter
    @Optional
    private String elicitation;

    @Parameter
    @Optional
    private String confirmation;

    public SlotPrompt() {
        // Default constructor
    }

    public String getElicitation() {
        return elicitation;
    }

    public String getConfirmation() {
        return confirmation;
    }
}
