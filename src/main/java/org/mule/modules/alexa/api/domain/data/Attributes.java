/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.api.domain.data;

import org.mule.runtime.api.util.MultiMap;

public class Attributes {

    private MultiMap<String, String> headers;
    private int statusCode;

    public MultiMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(MultiMap<String, String> headers) {
        this.headers = headers;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
