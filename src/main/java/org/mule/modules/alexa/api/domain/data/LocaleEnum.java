/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.api.domain.data;

import com.fasterxml.jackson.annotation.JsonValue;

public enum LocaleEnum {

	DE_DE("de-DE"),
	EN_AU("en-AU"),
	EN_CA("en-CA"), EN_GB("en-GB"),
	EN_IN("en-IN"), EN_US("en-US"),
	ES_ES("es-ES"), ES_MX("es-MX"),
	ES_US("es-US"), FR_CA("fr-CA"),
	FR_FR("fr-FR"), HI_IN("hi-IN"),
	IT_IT("it-IT"), JA_JP("ja-JP"),
	PT_BR("es-ES");

	private final String value;

	LocaleEnum(String value){
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return value;
	}
	
}
