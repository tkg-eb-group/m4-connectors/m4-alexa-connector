/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.intents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import java.io.Serializable;
@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class Variation implements Serializable {

	@Parameter
	private String type;

	@Parameter
	private String value;

	public Variation() {
		// Default constructor
	}

	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
}
