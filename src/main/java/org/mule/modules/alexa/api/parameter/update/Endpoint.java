/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */


package org.mule.modules.alexa.api.parameter.update;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import java.io.Serializable;

@JsonIgnoreProperties({
		"representation",
		"annotations",
		"rootContainerLocation",
		"dslSource",
		"location",
		"identifier"
})
public class Endpoint implements Serializable {

	@Parameter
	@Optional
	private String uri;

	@Parameter
	@Optional
	private String sslCertificateType;

	public Endpoint() {
		// Public constructor
	}

	public String getUri() {
		return uri;
	}

	public String getSslCertificateType() {
		return sslCertificateType;
	}
}
