/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.api.parameter.update;

import java.io.Serializable;

public class RegionEndpoint implements Serializable {

    public RegionEndpoint() {
        // Default constructor
    }

    private Endpoint endpoint;

    public Endpoint getEndpoint() {
        return endpoint;
    }
}
