/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.modules.alexa.util;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.Optional.ofNullable;

import java.io.*;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlexaLoadProperties {
    private static final Logger logger = LoggerFactory.getLogger(AlexaLoadProperties.class);

    protected static Properties getAlexaProperties() throws IOException {
        Properties properties = new Properties();
        String propertiesFile = format("%s/src/test/resources/%s", getProperty("user.dir"),
                ofNullable(getProperty("application.properties"))
                        .orElse("application.properties"));
        try (InputStream inputStream = new FileInputStream(propertiesFile)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new FileNotFoundException(format("property file '%s' not found in the classpath", propertiesFile));
        }
        logger.debug("properties loaded");
        logger.debug(properties.toString());
        return properties;
    }


    protected static void setAlexaProperties(Properties properties) throws IOException{
        String propertiesFile = format("%s/src/test/resources/%s", getProperty("user.dir"),
                ofNullable(getProperty("application.properties"))
                        .orElse("application.properties"));
        try (OutputStream outputStream = new FileOutputStream(propertiesFile)) {
            properties.store(outputStream, null);
        } catch (IOException e) {
            throw new FileNotFoundException(format("property file '%s' not found in the classpath", propertiesFile));
        }
        logger.debug("properties stored");
        logger.debug(properties.toString());
    }

}
