/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.alexa.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.mule.modules.alexa.model.AuthResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class AlexaPropertiesUtil {

	public static boolean setup() throws IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		Properties alexaProperties = AlexaLoadProperties.getAlexaProperties();

		HttpPost post = new HttpPost("https://api.amazon.com/auth/o2/token");

		List<NameValuePair> data = new ArrayList<>();
		data.add(new BasicNameValuePair("grant_type", "refresh_token"));
		data.add(new BasicNameValuePair("client_id", alexaProperties.getProperty("alexa.consumerKey")));
		data.add(new BasicNameValuePair("client_secret", alexaProperties.getProperty("alexa.consumerSecret")));
		data.add(new BasicNameValuePair("refresh_token", alexaProperties.getProperty("alexa.refresh.token")));

		post.setEntity(new UrlEncodedFormEntity(data));
		HttpResponse httpResponse = httpClient.execute(post);
		String response = EntityUtils.toString(httpResponse.getEntity());
		AuthResponse authResponse = new ObjectMapper().readValue(response, AuthResponse.class);
		alexaProperties.setProperty("alexa.token", authResponse.getAccessToken());
		AlexaLoadProperties.setAlexaProperties(alexaProperties);
		return true;
	}


	public static String getToken() throws Exception {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		Properties alexaProperties = AlexaLoadProperties.getAlexaProperties();

		HttpPost post = new HttpPost("https://api.amazon.com/auth/o2/token");

		List<NameValuePair> data = new ArrayList<NameValuePair>();
		data.add(new BasicNameValuePair("grant_type", "refresh_token"));
		data.add(new BasicNameValuePair("client_id", alexaProperties.getProperty("alexa.consumerKey")));
		data.add(new BasicNameValuePair("client_secret", alexaProperties.getProperty("alexa.consumerSecret")));
		data.add(new BasicNameValuePair("refresh_token", alexaProperties.getProperty("alexa.refresh.token")));
		post.setEntity(new UrlEncodedFormEntity(data));
		HttpResponse httpResponse = httpClient.execute(post);
		String response = EntityUtils.toString(httpResponse.getEntity());
		AuthResponse authResponse = new ObjectMapper().readValue(response, AuthResponse.class);


		return authResponse.getAccessToken();

	}

}

